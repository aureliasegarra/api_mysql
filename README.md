[![Archi](https://img.shields.io/badge/Microservices-gray?style=for-the-badge)](https://)
[![Front](https://img.shields.io/badge/FastAPI-34D873?style=for-the-badge&logoColor=white)](https://)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=project-46788851-bot-067f7c3c21e77f3e47a9268c0493fc03_api-mysql-sonar&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=project-46788851-bot-067f7c3c21e77f3e47a9268c0493fc03_api-mysql-sonar)

<br/>
<div align="center">
    <img src="logo-api.png" alt="Logo" width="80" height="80">
    <h1 align="center"><strong>FNUC API</strong></h1>
</div>
  <p align="center">
    Conception d'une API avec FastAPI et MySQL
    <br />
  </p>


<br/>
<br/>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>

  <ol>
    <li><a href="#description">Description</a></li>
    <li><a href="#schema">Schéma</a></li>
    <li><a href="#langages">Langages/Outils</a></li>
        <ul>
          <li><a href="#langages">API</a></li>
          <li><a href="#langages">BDD</a></li>
          <li><a href="#langages">IDE</a></li>
        </ul>
    <li><a href="#objectifs">Objectifs</a></li>
    <li><a href="#launch">Se connecter à l'API</a></li>
    <li><a href="#statut">Statut</a></li>
    <li><a href="#contexte">Contexte</a></li>
    <li><a href="#equipe">Equipe</a></li>
  </ol>
</details>

<br>
<br>




## 📒 Description <a id="description"></a>
Refonte d'une application de gestion de livres en micro services.
L'application est composée de 4 micro services :
- API-BDD MysSQL
- API-BDD MongoDB
- Authentification
- Web Client React

Nous travaillons sur la partie API-BDD MySQL.

## ✏️ Schéma <a id="schema"></a>
<img src="./images/screenshot-schema.png" alt="Illustration schéma">

## ⚙️ Langages/Outils <a id="langages"></a>
### API
- Python
- FastAPI
- ElasticSearch

### BDD
- MysSQL
- ORM - SQLAlchemy
- Pydantic

### IDE
- PyCharm

### TESTS
<details close="close">
    <summary>Test API avec POSTMAN</summary>

- Présentation du workspace API_MYSQL
<br>
<br>
<img src="./images/screenshot-postman_1.png" alt="Illustration postman">
<img src="./images/screenshot-postman_2.png" alt="Illustration postman">
<br>
<br>
- Variables pour l'environnement de développement
<br>
<br>
<img src="./images/screenshot-postman_3.png" alt="Illustration postman">

 </details>


## 🎯 Objectifs <a id="objectifs"></a>
- Mise en place d'une API python avec FastAPI
- Mise en place d'une BDD MySQL et connexion avec l'API
- Mise en place d'une recherche avec ElasticSearch
- Gestion d'une application en micro services
- Gestion de projet en méthode agile


## 🚀 Se connecter à l'API
### 1. Lancer Docker Desktop
### 2. Lancer Docker Compose
À la racine du projet
```
docker-compose up -d --build
```
### 3. Lancer le serveur de dev en local
```
uvicorn src.app.main:app --reload 
```
L'API est accessible à l'adresse : http://localhost:8000
### 4. Lancer le populate pour remplir la BDD
```
python src/populate.py
```


## 🚦 Statut <a id="statut"></a>

API est en cours de développement.


## 🏫 Contexte <a id="contexte"> </a>

C’est un projet qui s’inscrit dans notre parcours de formation de 
Licence DLIS (Développement Logiciel Innovant et Sécurisé), promotion 2022-2023.



## 🧑‍🤝‍🧑 Equipe <a id="equipe"> </a>

Ce projet a été conçu par :
- Maxime Archenoul - Scrum Master
- Sacha Farinel - Architecte
- Clément - Product Owner
- Sébastien Degasne - Développeur
- Aurelia Segarra - QA Testeur
