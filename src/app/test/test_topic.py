import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from ..models import Topic

class TopicTestCase(unittest.TestCase):
    def setUp(self):
        # Configurer la connexion à la base de données de test
        engine = create_engine('sqlite:///test.db')
        Session = sessionmaker(bind=engine)
        self.session = Session()

        # Créer la table "topics" dans la base de données de test
        Topic.metadata.create_all(engine)

    def tearDown(self):
        # Supprimer la table "topics" de la base de données de test
        Topic.metadata.drop_all(self.session.bind)

    def test_topic_representation(self):
        # Créer un sujet de test
        topic = Topic(
            label="Science",
            topic_url="https://example.com/science"
        )
        self.session.add(topic)
        self.session.commit()

        # Vérifier que la représentation en chaîne de caractères est correcte
        expected_string = "Topic : 1 - Science - https://example.com/science"
        self.assertEqual(repr(topic), expected_string)

if __name__ == '__main__':
    unittest.main()