import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ..models import Book


class BookTestCase(unittest.TestCase):
    def setUp(self):
        # Configurer la connexion à la base de données de test
        engine = create_engine('sqlite:///test.db')
        Session = sessionmaker(bind=engine)
        self.session = Session()

        # Créer la table "books" dans la base de données de test

        Book.metadata.create_all(engine)

    def tearDown(self):
        # Supprimer la table "books" de la base de données de test
        Book.metadata.drop_all(self.session.bind)

    def test_book_string_representation(self):
        # Créer un livre de test
        book = Book(
            title="Titre du livre",
            author="Auteur du livre",
            resume="Résumé du livre",
            cover_url="URL de la couverture",
            price=10,
            stock=50
        )
        self.session.add(book)
        self.session.commit()

        # Vérifier que la représentation en chaîne de caractères est correcte
        expected_string = "Book : 1 - Titre du livre - Auteur du livre - 10"
        self.assertEqual(str(book), expected_string)

if __name__ == '__main__':
    unittest.main()