import os
from loguru import logger

""" Middleware to log errors in a file """
# Path to the log file
log_file_path = os.path.join(os.path.dirname(__file__), "..", "logs", "app.log")
# Create the log file if it does not exist
logger.add(log_file_path, level="INFO", format="{time} - {level} - {message}")


async def log_middleware(request, call_next):
    """
        Middleware to log errors in a file

        :param request: request
        :param call_next: call_next
        :return: response
    """
    logger.info(f"Request: {request.method} {request.url.path}")
    response = await call_next(request)

    return response
