from pydantic import BaseModel


class BookTopic(BaseModel):
    id_book: int
    id_topic: int

    class Config:
        orm_mode = True
