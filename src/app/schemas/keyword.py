from pydantic import BaseModel


class KeywordBase(BaseModel):
    label: str


class Keyword(KeywordBase):
    id: int

    class Config:
        orm_mode = True

