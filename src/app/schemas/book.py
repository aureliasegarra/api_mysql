from pydantic import BaseModel, validator


class BookBase(BaseModel):
    title: str
    author: str
    resume: str = ''
    image: str = ''
    price: float = 1.0
    stock: int = 0

    @validator('price')
    def validate_price(cls, price):
        if price <= 0:
            raise ValueError("Price must be greater than zero.")
        return price


class BookTopic(BaseModel):
    id_book: int
    id_topic: int


class BookKeyword(BaseModel):
    id_book: int
    id_keyword: int


class Book(BookBase):
    id: int

    class Config:
        orm_mode = True
