from typing import Annotated

from fastapi import APIRouter, Depends
from fastapi.params import Path
from sqlalchemy.orm import Session

from ..config import get_db
from ..crud import get_one_by_id, get_all, patch_one_by_id, post_one, delete_one_by_id, get_one_relationship_by_id, \
    get_all_relationships_by_id
from ..internal import JWTBearer
from ..internal import is_admin
from ..models import Topic, Customer
from ..schemas import Topic as TopicModel, TopicBase, Book

router = APIRouter()


@router.post('/', response_model=TopicModel)
def create_topic(topic: TopicBase,
                 current_user: Annotated[Customer, Depends(JWTBearer())],
                 db: Session = Depends(get_db)
                 ):
    """
        Create a new topic. Only admins can create topics.

        :param topic: The topic to create
        :param current_user: The current user token
        :param db: The database session
    """
    if is_admin(current_user):
        return post_one(model=Topic, db=db, **topic.dict())


@router.delete('/{topic_id}', response_model=TopicModel)
def delete_topic(topic_id: Annotated[int, Path(title="The ID of the topic to delete")],
                 current_user: Annotated[Customer, Depends(JWTBearer())],
                 db: Session = Depends(get_db)
                 ):
    """
        Delete a topic by ID. Only admins can delete topics.

        :param topic_id: The ID of the topic to delete
        :param current_user: The current user token
        :param db: The database session
    """
    if is_admin(current_user):
        return delete_one_by_id(id=topic_id, model=Topic, db=db)


@router.get('/', response_model=list[TopicModel])
def get_all_topics(limit: int = 100, offset: int = 0, db: Session = Depends(get_db)):
    """
        Get all topics.

        :param limit: The maximum number of topics to get
        :param offset: The number of topics to skip
        :param db: The database session
    """
    return get_all(model=Topic, db=db, limit=limit, offset=offset)


@router.get('/{topic_id}', response_model=TopicModel)
def get_topic(topic_id: Annotated[int, Path(title="The ID of the topic to get")], db: Session = Depends(get_db)):
    """
        Get a topic by ID.

        :param topic_id: The ID of the topic to get
        :param db: The database session
    """
    return get_one_by_id(id=topic_id, model=Topic, db=db)


@router.get("/{topic_id}/books/{book_id}", response_model=Book)
def get_topic_book(topic_id: int, book_id: int, db: Session = Depends(get_db)):
    """
        Get a book by ID and topic ID.

        :param topic_id: The ID of the topic to get
        :param book_id: The ID of the book to get
        :param db: The database session
    """
    return get_one_relationship_by_id(db=db, id=topic_id, external_id=book_id, model=Topic,
                                      relationship_field='books')


@router.get("/{topic_id}/books", response_model=list[Book])
def get_topic_books(topic_id: int, db: Session = Depends(get_db)):
    """
        Get all books by topic ID.

        :param topic_id: The ID of the topic to get
        :param db: The database session
    """
    return get_all_relationships_by_id(db=db, id=topic_id, model=Topic, relationship_field='books')


@router.patch('/{topic_id}', response_model=TopicModel)
def patch_topic(topic_id: Annotated[int, Path(title="The ID of the topic to patch")],
                topic: TopicBase,
                current_user: Annotated[Customer, Depends(JWTBearer())],
                db: Session = Depends(get_db)
                ):
    """
        Patch a topic by ID. Only admins can patch topics.

        :param topic_id: The ID of the topic to patch
        :param topic: The topic to patch
        :param current_user: The current user token
        :param db: The database session
    """
    if is_admin(current_user):
        return patch_one_by_id(id=topic_id, model=Topic, db=db, **topic.dict())
