import os
from typing import Annotated

import requests
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from ..internal import JWTBearer, call_auth
from ..crud import post_one
from ..config import get_db
from ..schemas import CustomerConfirmation
from ..schemas import CustomerLogin
from ..models import Customer

router = APIRouter()


@router.post('/register', response_class=JSONResponse)
def register_customer(customer_confirmation: CustomerConfirmation, db: Session = Depends(get_db)):
    """
        Register a new customer

        :param customer_confirmation: CustomerConfirmation
        :param db: Session
        :return: JSONResponse
    """
    [response, response_content] = call_auth(method='post', path='register', json=customer_confirmation.dict())
    if response.status_code == 201:
        token = response_content['token']
        # Get user information
        [_, response_content_get_user] = call_auth(method='get', path='user', token_user=token)
        id_customer = {'id_auth': response_content_get_user['id']}
        # Create a new customer in our database
        post_one(model=Customer, db=db, **id_customer)
    return JSONResponse(content=response_content, status_code=response.status_code)


@router.post('/login', response_class=JSONResponse)
def login_customer(customer: CustomerLogin):
    """
        Login a customer

        :param customer: CustomerLogin
        :return: JSONResponse
    """
    [response, response_content] = call_auth(method='post', path='login', json=customer.dict())
    return JSONResponse(content=response_content, status_code=response.status_code)


@router.post('/logout', response_class=JSONResponse)
def logout_customer(token_user: Annotated[str, Depends(JWTBearer())]):
    """
        Logout a customer

        :param token_user: str
        :return: JSONResponse
    """
    [response, response_content] = call_auth(method='patch', path='logout', token_user=token_user['token'])
    return JSONResponse(content=response_content, status_code=response.status_code)
