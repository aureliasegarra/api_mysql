from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException
from fastapi.params import Path
from sqlalchemy.orm import Session

from ..config import get_db
from ..crud import get_one_by_id, get_all, patch_one_by_id, post_one, delete_one_by_id, patch_one_by_id_auth
from ..internal import JWTBearer, get_current_customer
from ..models import Customer, Book, Customer
from ..models import Order
from ..schemas import Order as OrderModel, OrderBase

router = APIRouter()


@router.post('/', response_model=OrderModel)
def create_order(order: OrderBase,
                 current_user: Annotated[Customer, Depends(get_current_customer)],
                 token_user: Annotated[str, Depends(JWTBearer())],
                 db: Session = Depends(get_db)
                 ):
    """
        Create a new order.

        Args:
            order: The order to create
            current_user: The current user
            token_user: The user from the token
            db: The database session
    """
    book = get_one_by_id(id=order.id_book, model=Book, db=db)
    stock = book.stock - order.quantity

    if stock <= 0:
        raise HTTPException(status_code=500, detail=str("Not enough stock."))

    patch_one_by_id(id=book.id, model=Book, db=db, stock=stock)

    new_order = post_one(model=Order, db=db, **order.dict())

    total_price = book.price * order.quantity
    current_user['cumulative_sales'] += total_price

    patch_one_by_id_auth(id=current_user['id'],
                         model=Customer,
                         db=db,
                         cumulative_sales=current_user['cumulative_sales']
                         )

    return new_order


@router.delete('/{order_id}', response_model=OrderModel)
def delete_order(order_id: Annotated[int, Path(title="The ID of the order to delete")],
                 current_user: Annotated[Customer, Depends(JWTBearer())], db: Session = Depends(get_db)):
    """
        Delete an order

        Args:
            order_id: The ID of the order to delete
            current_user: The current user (must be admin)
            db: The database session
    """
    return delete_one_by_id(id=order_id, model=Order, db=db)


@router.get('/{order_id}', response_model=OrderModel)
def get_order(order_id: Annotated[int, Path(title="The ID of the order to get")],
              current_user: Annotated[Customer, Depends(JWTBearer())], db: Session = Depends(get_db)):
    """
        Get an order by ID.

        Args:
            order_id: The ID of the order to get
            current_user: The current user (must be admin)
            db: The database session
    """
    return get_one_by_id(id=order_id, model=Order, db=db)


@router.get('/', response_model=list[OrderModel])
def get_all_orders(limit: int = 100, offset: int = 0, db: Session = Depends(get_db)):
    """
        Get all orders.

        Args:
            limit: The maximum number of orders to get
            offset: The offset
            db: The database session
    """
    return get_all(model=Order, db=db, limit=limit, offset=offset)


@router.patch('/{order_id}', response_model=OrderModel)
def patch_order(order_id: Annotated[int, Path(title="The ID of the order to patch")], order: OrderBase,
                current_user: Annotated[Customer, Depends(JWTBearer())],
                db: Session = Depends(get_db)):
    """
        Patch an order by ID.

        Args:
            order_id: The ID of the order to patch
            order: The order to patch
            current_user: The current user (must be admin)
            db: The database session
    """
    return patch_one_by_id(id=order_id, model=Order, db=db, **order.dict())
