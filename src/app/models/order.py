from sqlalchemy import Column, Integer, DateTime, ForeignKey, String
from sqlalchemy.orm import relationship

from ..config import Base


class Order(Base):
    __tablename__ = "orders"

    id = Column(Integer, primary_key=True, index=True)
    id_book = Column(Integer, ForeignKey("books.id"))
    id_customer = Column(String(255), ForeignKey("customers.id_auth"))
    date = Column(DateTime)
    quantity = Column(Integer)

    books = relationship("Book", back_populates="orders")
    customers = relationship("Customer", back_populates="orders")

    def __repr__(self):
        return f"Order : {self.id} - {self.quantity} - {self.date}"
