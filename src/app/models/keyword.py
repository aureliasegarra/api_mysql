from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from . import book_keyword
from ..config import Base


class Keyword(Base):
    __tablename__ = "keywords"

    id = Column(Integer, primary_key=True, index=True)
    label = Column(String(255))

    books = relationship("Book", secondary=book_keyword, back_populates="keywords")

    def __repr__(self):
        return f"Keyword : {self.id} - {self.label}"
