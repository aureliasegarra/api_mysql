from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .routers import customer, book, topic, keyword, order, auth

try:
    from .middlewares import log_middleware, error_log_middleware
    from apscheduler.schedulers.background import BackgroundScheduler
    from .elastic.functions.esfunctions import es_load_customers_script, es_load_books_script
    from .routers import es
except ImportError:
    log_middleware = None
    error_log_middleware = None
    BackgroundScheduler = None
    es_load_customers_script = None
    es_load_books_script = None
    es = None

tags_metadata = [
    {
        "name": "auth",
        "description": "Operations with authentification",
    },
    {
        "name": "keyword",
        "description": "Operations with keywords",
    },
    {
        "name": "topic",
        "description": "Operations with topic",
    },
    {
        "name": "book",
        "description": "Operations with book",
    },
    {
        "name": "order",
        "description": "Operations with orders",
    },
    {
        "name": "customer",
        "description": "Operations with customers",
    },
    {
        "name": "elastic search",
        "description": "Operations with elastic search",
    },
]

title = 'FNUC'
version = "0.0.1"
servers = [
    {'url': 'http://localhost', 'description': 'Local'},
    {'url': 'https://apimysql-1-r1261081.deta.app', 'description': 'Prod'}
]

app = FastAPI(
    title='FNUC',
    version="0.0.1",
    openapi_tags=tags_metadata,
    root_path_in_servers=True,
    servers=servers
)

origins = [
    '*'
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth.router, prefix="/auth", tags=['auth'])
app.include_router(book.router, prefix="/books", tags=['book'])
app.include_router(customer.router, prefix="/customers", tags=['customer'])
app.include_router(keyword.router, prefix="/keywords", tags=['keyword'])
app.include_router(order.router, prefix="/orders", tags=['order'])
app.include_router(topic.router, prefix="/topics", tags=['topic'])

if log_middleware is not None:
    app.middleware("http")(log_middleware)

if error_log_middleware is not None:
    app.middleware("http")(error_log_middleware)


@app.get("/")
async def root():
    return {"message": "FNUC - MySQL API"}


if BackgroundScheduler is not None and es_load_customers_script is not None and es_load_books_script is not None:
    app.include_router(es.router, prefix="/es", tags=['elastic search'])


    @app.on_event("startup")
    def index_all_es():
        # es_load_customers_script()
        es_load_books_script()
        scheduler = BackgroundScheduler()
        # scheduler.add_job(es_load_customers_script, 'cron', minute='*/1')
        scheduler.add_job(es_load_books_script, 'cron', second='*/15')
        scheduler.start()
