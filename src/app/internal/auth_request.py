import os
import requests

from requests.exceptions import JSONDecodeError
from fastapi import HTTPException

""" Call authentification service """


def call_auth(method: str, path: str, token_user: str = None, json: dict = None):
    """
        Call authentification service

        :param method: http method
        :param path: path of the request
        :param token_user: token of the user
        :param json: json of the request
        :return: response and response content
    """
    request_method = getattr(requests, method)

    if token_user is not None:
        headers = {'Authorization': 'Bearer ' + token_user}
    else:
        headers = None

    if request_method is not None:
        response = request_method(os.environ['AUTH_URL'] + '/' + path, headers=headers, json=json)
        try:
            response_content = response.json()
        except JSONDecodeError:
            response_content = response.text
        return [response, response_content]
    else:
        raise HTTPException(status_code=500, detail="Undefined http method")
