from pydantic import BaseSettings, Field, SecretStr
from elasticsearch import Elasticsearch

""" This file contains the Elasticsearch settings. """


class ESAuth(BaseSettings):
    """ Elasticsearch authentication settings."""
    host: str = Field(env="ES_HOST", default="http://localhost:9200")
    user: str = Field(env="ES_USER", default="elastic")
    password: SecretStr = Field(env="ES_PASSWORD", default="elastic")


es_auth = ESAuth()

es_client = Elasticsearch(
    es_auth.host,
    basic_auth=(es_auth.user, es_auth.password.get_secret_value()),
)

MYSQL_HOST = "localhost"
MYSQL_USER = "root"
MYSQL_PASSWORD = "root"
MYSQL_DATABASE = "fnuc"
