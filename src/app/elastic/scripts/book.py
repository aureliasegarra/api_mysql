""" Elastic search book index settings and mappings """
BOOK_INDEX_NAME = "book"
BOOK_INDEX_SETTINGS = {
    "analysis": {
        "analyzer": {
            "post_index_analyzer": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": [
                    "lowercase",
                    "autocomplete_filter",
                ],
            },
            "post_search_analyzer": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": [
                    "lowercase",
                ],
            },
        },
        "filter": {
            "autocomplete_filter": {
                "type": "edge_ngram",
                "min_gram": 1,
                "max_gram": 20,
            },
        },
    },
}
BOOK_INDEX_MAPPINGS = {
    "properties": {
        "title": {
            "type": "text",
            "search_analyzer": "post_search_analyzer",
            "fields": {
                "ngrams": {
                    "type": "text",
                    "analyzer": "post_index_analyzer",
                    "search_analyzer": "post_search_analyzer",
                },
            },
        },
        "author": {
            "type": "text",
            "search_analyzer": "post_search_analyzer",
            "fields": {
                "ngrams": {
                    "type": "text",
                    "analyzer": "post_index_analyzer",
                    "search_analyzer": "post_search_analyzer",
                },
            },
        },
        "keyword_labels": {
            "type": "text",
            "search_analyzer": "post_search_analyzer",
        },
        "topic_labels": {
            "type": "text",
            "search_analyzer": "post_search_analyzer",
        }
    }
}
