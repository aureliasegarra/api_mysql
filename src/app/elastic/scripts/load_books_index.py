import argparse
import logging
import elasticsearch
import mysql.connector
import sys
from book import *
from es import *

logging.basicConfig(level=logging.INFO)


def get_books_from_database():
    """ Fetches all books from the database."""
    try:
        cnx = mysql.connector.connect(
            host=MYSQL_HOST,
            user=MYSQL_USER,
            password=MYSQL_PASSWORD,
            database=MYSQL_DATABASE,
        )
        cursor = cnx.cursor(dictionary=True)

        query = """
                    SELECT books.*, GROUP_CONCAT(DISTINCT keywords.label) AS keyword_labels, GROUP_CONCAT(DISTINCT topics.label)
                    AS topic_labels
                    FROM books
                    JOIN book_keyword ON book_keyword.id_book = books.id
                    JOIN keywords ON keywords.id = book_keyword.id_keyword
                    JOIN book_topic ON book_topic.id_book = books.id
                    JOIN topics ON topics.id = book_topic.id_topic
                    GROUP BY books.id
                """

        cursor.execute(query)
        books = cursor.fetchall()

        cursor.close()
        cnx.close()

        return books
    except mysql.connector.errors.ProgrammingError as exc:
        logging.error("An error occurred while fetching books from the database: %s", exc)
        return []


def recreate_index():
    """ (Re-)creates the Elasticsearch index."""
    try:
        es_client.indices.delete(index=BOOK_INDEX_NAME)
        logging.info("Index '%s' is deleted if existing.", BOOK_INDEX_NAME)
    except elasticsearch.exceptions.NotFoundError:
        logging.info("Index '%s' does not exist.", BOOK_INDEX_NAME)
    except elasticsearch.exceptions.ElasticsearchException as exc:
        logging.error("An error occurred while deleting the index: %s", exc)
        return

    try:
        es_client.indices.create(
            index=BOOK_INDEX_NAME,
            settings=BOOK_INDEX_SETTINGS,
            mappings=BOOK_INDEX_MAPPINGS,
        )
        logging.info("Index '%s' is (re-)created.", BOOK_INDEX_NAME)
    except elasticsearch.exceptions.ElasticsearchException as exc:
        logging.error("An error occurred while creating the index: %s", exc)


def load_documents_to_index():
    """ Loads all books from the database and indexes them."""
    books = get_books_from_database()

    if not books:
        logging.warning("No books found in the database.")
        return

    es_actions = []
    for book in books:
        book_id = book["id"]
        keyword_labels = book["keyword_labels"].split(",") if book["keyword_labels"] else []
        topic_labels = book["topic_labels"].split(",") if book["topic_labels"] else []

        document = {
            "id": book_id,
            "title": book["title"],
            "author": book["author"],
            "keyword_labels": keyword_labels,
            "topic_labels": topic_labels,
            "image": book["image"],
            "price": book["price"],
            "stock": book["stock"],
        }

        action = {"index": {"_index": BOOK_INDEX_NAME, "_id": book_id}}
        es_actions.append(action)
        es_actions.append(document)

    es_client.bulk(
        index=BOOK_INDEX_NAME,
        operations=es_actions,
        filter_path="took,errors",
    )

    logging.info("%s books have been indexed.", len(books))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--recreate",
        dest="recreate_index",
        action="store_true",
        help="""
            If True, the old index will be deleted if existing before a
            new one is created.
            """,
    )

    args = parser.parse_args()

    if (
            not es_client.indices.exists(index=BOOK_INDEX_NAME)
            or args.recreate_index
    ):
        try:
            recreate_index()
        except Exception as exc:
            logging.exception(exc)

    try:
        load_documents_to_index()
    except Exception as exc:
        logging.exception(exc)
    finally:
        es_client.close()
