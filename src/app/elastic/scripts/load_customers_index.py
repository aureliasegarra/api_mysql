import argparse
import logging
import sys
import mysql.connector
from customer import *
from es import *


def get_customers_from_database():
    """ Fetch customer data from the database."""
    cnx = mysql.connector.connect(
        host=MYSQL_HOST,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        database=MYSQL_DATABASE,
    )
    cursor = cnx.cursor(dictionary=True)

    query = "SELECT * FROM customers"
    cursor.execute(query)
    customers = cursor.fetchall()

    cursor.close()
    cnx.close()

    return customers


def recreate_index():
    """ (Re-)creates the Elasticsearch index."""
    es_client.options(ignore_status=[400, 404]).indices.delete(index=CUSTOMER_INDEX_NAME)
    logging.info("Index '%s' is deleted if existing.", CUSTOMER_INDEX_NAME)
    es_client.indices.create(
        index=CUSTOMER_INDEX_NAME,
        settings=CUSTOMER_INDEX_SETTINGS,
        mappings=CUSTOMER_INDEX_MAPPINGS
    )
    logging.info("Index '%s' is (re-)created.", CUSTOMER_INDEX_NAME)


def load_documents_to_index():
    """ Loads all customers from the database and indexes them."""
    customers = get_customers_from_database()

    es_actions = []
    for customer in customers:
        customer_id = customer["id"]
        customer.pop("hashed_password")

        action = {"index": {"_index": CUSTOMER_INDEX_NAME, "_id": customer_id}}
        es_actions.append(action)
        es_actions.append(customer)

    es_client.bulk(
        index=CUSTOMER_INDEX_NAME,
        operations=es_actions,
        filter_path="took,errors",
    )

    logging.info("%s customers have been indexed.", len(customers))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--recreate",
        dest="recreate_index",
        action="store_true",
        help="""
            If True, the old index will be deleted if existing before a
            new one is created.
            """,
    )

    args = parser.parse_args()

    if (
            not es_client.indices.exists(index=CUSTOMER_INDEX_NAME)
            or args.recreate_index
    ):
        try:
            recreate_index()
        except Exception as exc:
            logging.exception(exc)

    try:
        load_documents_to_index()
    except Exception as exc:
        logging.exception(exc)
    finally:
        es_client.close()
