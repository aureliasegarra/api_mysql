import os
import subprocess
from elasticsearch import NotFoundError

from ..scripts.es import es_client

DIR = os.path.dirname(os.path.abspath(__file__))

""" This file contains the Elasticsearch functions."""


def convert_model_to_dict(model):
    """ Converts a SQLAlchemy model to a dictionary. """
    data = model.__dict__.copy()
    data.pop('_sa_instance_state', None)
    return data


def es_load_customers_script():
    """ Runs the load_customers_index.py script. """
    script_path = os.path.join(DIR, "..", "scripts", "load_customers_index.py")
    subprocess.run(["python", script_path], check=True)


def es_load_books_script():
    """ Runs the load_books_index.py script. """
    script_path = os.path.join(DIR, "..", "scripts", "load_books_index.py")
    subprocess.run(["python", script_path], check=True)


def es_add_customer(data):
    """ Adds a customer to the Elasticsearch index."""
    updated_data = convert_model_to_dict(data)
    updated_data.pop('hashed_password', None)
    es_client.index(index="customer", id=data.id, body=updated_data)


def es_update_customer(_id, updated_customer):
    """ Updates a customer in the Elasticsearch index."""
    updated_data = convert_model_to_dict(updated_customer)
    updated_data.pop('hashed_password', None)
    updated_body = {
        "doc": updated_data
    }
    es_client.update(
        index="customer",
        id=_id,
        body=updated_body
    )


def es_delete_customer(_id):
    """ Deletes a customer from the Elasticsearch index."""
    try:
        es_client.delete(index="customer", id=_id)
    except NotFoundError:
        pass


def es_add_book(data):
    """ Adds a book to the Elasticsearch index. """
    updated_data = convert_model_to_dict(data)
    es_client.index(index="book", id=data.id, body=updated_data)


def es_update_book(_id, updated_book):
    """ Updates a book in the Elasticsearch index. """
    updated_data = convert_model_to_dict(updated_book)
    updated_body = {
        "doc": updated_data
    }
    es_client.update(
        index="book",
        id=_id,
        body=updated_body
    )


def es_delete_book(_id):
    """ Deletes a book from the Elasticsearch index. """
    try:
        es_client.delete(index="book", id=_id)
    except NotFoundError:
        pass
