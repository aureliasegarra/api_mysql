from datetime import datetime
import random
from faker import Faker
from sqlalchemy import insert
from sqlalchemy.orm import Session

from app.config.database import SessionLocal, engine
from app.models.book import Book
from app.models.book_keyword import book_keyword
from app.models.book_topic import book_topic
from app.models.customer import Customer, Base
from app.models.keyword import Keyword
from app.models.order import Order
from app.models.topic import Topic
from app.schemas.customer import CustomerConfirmation
from app.routers.auth import register_customer
from app.crud.base import get_all

fake = Faker()


def create_tables():
    print("Creating tables...")
    Base.metadata.create_all(bind=engine)
    print("Tables created.")


def drop_tables():
    print("Dropping tables...")
    Base.metadata.drop_all(bind=engine)
    print("Tables dropped.")


def get_printed_deployement(db):
    return print(str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + " - " + str(db))


def populate():
    drop_tables()
    create_tables()
    populate_keyword(SessionLocal())
    populate_topic(SessionLocal())
    populate_book(SessionLocal())
    populate_customer(SessionLocal())
    populate_order(SessionLocal())


def populate_book(db: Session):
    print("Populating book...")
    for _ in range(1, 100):
        db_book = Book(title=fake.text(15), author=fake.name(), resume=fake.text(), image=fake.url(),
                       price=fake.random_int(), stock=fake.random_int())
        db.add(db_book)
        db.commit()
        db.refresh(db_book)
        get_printed_deployement(db_book)

        populate_book_topic(db, db_book.id)
        populate_book_keyword(db, db_book.id)


def populate_book_keyword(db: Session, book_id: int):
    num_keywords = fake.random_int(min=1, max=5)
    keywords_ids = fake.random_elements(elements=range(1, 100), length=num_keywords, unique=True)

    book_keyword_values = [{"id_book": book_id, "id_keyword": keyword_id} for keyword_id in keywords_ids]
    db.execute(insert(book_keyword), book_keyword_values)
    db.commit()


def populate_book_topic(db: Session, book_id: int):
    num_topics = fake.random_int(min=1, max=5)
    topic_ids = fake.random_elements(elements=range(1, 100), length=num_topics, unique=True)

    book_topics_values = [{"id_book": book_id, "id_topic": topic_id} for topic_id in topic_ids]
    db.execute(insert(book_topic), book_topics_values)
    db.commit()


def populate_customer(db: Session):
    print("Populating customer...")
    for i in range(1, 10):
        password = fake.password()
        customer = CustomerConfirmation(email=fake.email(), firstname=fake.first_name(), lastname=fake.last_name(), password=password, password_confirmation=password)
        created_customer = register_customer(customer, db)
        get_printed_deployement(created_customer)


def populate_keyword(db: Session):
    print("Populating keyword...")
    for _ in range(1, 100):
        text_length = fake.random_int(min=5, max=10)
        random_text = fake.text(text_length)
        db_keyword = Keyword(label=random_text)
        db.add(db_keyword)
        db.commit()
        db.refresh(db_keyword)
        get_printed_deployement(db_keyword)


def populate_order(db: Session):
    print("Populating order...")
    customers = get_all(Customer, db)
    if customers:
        for _ in range(1, 100):
            random_customer = random.choice(customers)
            db_order = Order(
                date=fake.date_time(),
                id_book=fake.random_int(min=1, max=99),
                id_customer=random_customer.id_auth,
                quantity=fake.random_int(min=1, max=10)
            )
            db.add(db_order)
            db.commit()
            db.refresh(db_order)
            get_printed_deployement(db_order)


def populate_topic(db: Session):
    print("Populating topic...")
    for _ in range(1, 100):
        text_length = fake.random_int(min=5, max=10)
        random_text = fake.text(text_length)
        db_topic = Topic(label=random_text, image=fake.url())
        db.add(db_topic)
        db.commit()
        db.refresh(db_topic)
        get_printed_deployement(db_topic)


if __name__ == '__main__':
    populate()
