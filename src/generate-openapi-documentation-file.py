from fastapi.openapi.utils import get_openapi
from app import main
import json

print(
    json.dumps(
        get_openapi(
            title=main.title,
            version=main.version,
            routes=main.app.routes,
            tags=main.tags_metadata,
            servers=main.servers
        )
    )
)
